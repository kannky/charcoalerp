# -*- coding: utf-8 -*-

'''
Created on 2015. 10. 5.

@author: aplus02

'''
from django.db import models


class WareHouse(models.Model):
    """
    . 창고 
    
    """
    #창고 이름 
    name =  models.CharField(max_length=15,blank=True, null=True)
    
    
    #기타 메모사항 
    
    etc = model.TextField()
    
    
    


class InCome(models.Model):
    """
     . 입고장 


    """
    
    #송장번호 
    invoice_number = models.CharField(max_length=15,blank=True, null=True)
    
    #입고날짜 
    income_date = models.DateTimeField()
    
    #수입국가
    nation =  models.CharField(max_length=15,blank=True, null=True)
    
    #품목 
    spec =  models.CharField(max_length=15,blank=True, null=True)
    
    #수량 
    amount = models.IntegerField(blank=True, null=True)
    
    

class CharcoalBox(models.Model):
    """
    . 저장되는 박스 단위 
    
    
    """
    
    # 입고장 
    income = models.ForeignKey( InCome, related_name='charcoalbox',  max_length=12, blank=True, null=True)
    
    
    # 입고날짜
    
    
    #품명 
    
    
    #수량 
    
    
    #창고 
    
    
    


class Client(models.Model):
    """
    . 거래처 
    
    """
    
    
    
    
    
    
class OutComeBook(models.Model):
    """
    . 출고 장부 
    
    """
    
    
    
            
    
    